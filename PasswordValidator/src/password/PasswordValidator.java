package password;

/*
 * @author Jacob Bruce, 991 518 971
 * 
 * This class ensures a given string is a valid password
 * 
 * */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;

	public static boolean isValidLength(String password) {
		if(password == null)
			return false;
		
		return password.trim().length() >= MIN_LENGTH;
	}
	
	
	
	
	public static boolean hasEnoughDigits(String password) {
		int counter = 0;
		
		for(char c : password.toCharArray()) {
			if(Character.isDigit(c)) 
				counter++;
		}
		
		return counter >= MIN_DIGITS;
	}
	
	public static boolean hasUppercase(String password) {
		for(char c : password.toCharArray()) {
			if(Character.isUpperCase(c)) {
				return true;
			}
		}
		return false;
	}
	
}
