package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Jacob Bruce, 991 518 971
 * */


public class PasswordValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
	}
  
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("          "));
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567"));
	}
	
	@Test
	public void testHasEnoughDigitsRegular() {
		assertTrue("Invalid number of digits", PasswordValidator.hasEnoughDigits("pass123456"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Invalid number of digits", PasswordValidator.hasEnoughDigits("pass"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Invalid number of digits", PasswordValidator.hasEnoughDigits("pass12"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Invalid number of digits", PasswordValidator.hasEnoughDigits("pass1"));
	}
	
	@Test
	public void testHasUppercaseRegular() {
		assertTrue("Invalid number of uppercase letters", PasswordValidator.hasUppercase("PASSWORD"));
	}
	
	@Test
	public void testHasUppercaseException() {
		assertFalse("Invalid number of uppercase letters", PasswordValidator.hasUppercase(""));
	}
	
	@Test
	public void testHasUppercaseBoundaryIn() {
		assertTrue("Invalid number of uppercase letters", PasswordValidator.hasUppercase("Password"));
	}
	
	@Test
	public void testHasUppercaseBoundaryOut() {
		assertFalse("Invalid number of uppercase letters", PasswordValidator.hasUppercase("password"));
	}
	
}
